#include "DbConnection.h"

DbConnection* DbConnection::pDbConnection;

DbConnection::DbConnection(bool bIsOffline)
{
	
	bIsOfflineDatabase = bIsOffline;
	
	bConnectionEstablished = performConnection();
}

DbConnection::~DbConnection()
{
	
}

bool DbConnection::performConnection()
{
	if (bIsOfflineDatabase)
	{
		sDbPath = QString("/home/pi/Desktop/pipos_db.db");
		
		db = QSqlDatabase::addDatabase("QSQLITE");
		db.setDatabaseName(sDbPath);
	}
	else
	{
		sDatabaseName = "pipos_db";
		sHostName = "localhost";
		sUserName = "root";
		sPassword = "";
	
		db = QSqlDatabase::addDatabase("QMYSQL");

		db.setHostName(sHostName);
		db.setDatabaseName(sDatabaseName);
		db.setPort(3306);
		db.setUserName(sUserName);
		db.setPassword(sPassword);
		
	}
	

	if (!db.open())
	{
		//qDebug() << "Database connection failed!";
		return false;
	}
	else {
		return true;
	}
}

bool DbConnection::isConnected()
{
	return bConnectionEstablished;
}

QSqlDatabase DbConnection::getHandle()
{
	return db;
}

