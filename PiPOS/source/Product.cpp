#include "Product.h"

Product::Product()
{
	
}

Product::Product(unsigned int ui_ProductIdIn, QString s_DescIn, float f_PriceIn)
{
	ui_ProductId = ui_ProductIdIn;
	s_Description = s_DescIn;
	f_Price = f_PriceIn;

}

Product::~Product()
{
	
}

void Product::SetDescription(QString s_DescIn)
{
	s_Description = s_DescIn;
}

void Product::SetCategory(QString catIn)
{
	productCat = Converter::ConverterInstance()->ConvertCategoryFromString(catIn);
}

void Product::SetTaxCategory(TaxCategory taxCatIn)
{
	
}

void Product::SetPrice(float f_PriceIn)
{
	f_Price = f_PriceIn;
}
		
unsigned int Product::GetID()
{
	return ui_ProductId;
}

QString Product::GetDescription()
{
	return s_Description;
}

float Product::GetPrice()
{
	return f_Price;
}