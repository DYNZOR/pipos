#include "EntityFactory.h"

EntityFactory* EntityFactory::pEntityFactory;

void EntityFactory::StartupFactory()
{
	productsModel = new QSqlQueryModel();
	
	// Query the database for all products 
	QSqlQuery selectProductsQuery;
	selectProductsQuery.prepare("SELECT * FROM products");
	
	// Update the products table view 
	UpdateProductsModelView(selectProductsQuery);
	
	// Create a new order 
	CreateOrder(true);
	
}

void EntityFactory::ShutdownFactory()
{
	
}

void EntityFactory::CreateOrder(bool bIsNew)
{
	if (bIsNew)
	{
		Order* newOrder = new Order(QString("NEW"));
		p_ActiveOrder = newOrder;
	}
}
	
void EntityFactory::CreateProduct(const Product& newProduct)
{
	
}

void EntityFactory::CreateReceipt()
{
	
}

void EntityFactory::CreateReport()
{
	
}

void EntityFactory::UpdateProductsModelView(QSqlQuery queryIn)
{
	queryIn.exec();
	productsModel->setQuery(queryIn);
}

QSqlQueryModel* EntityFactory::GetProductsModel()
{
	return productsModel;
}

Order* EntityFactory::GetActiveOrder()
{
	return p_ActiveOrder;
}


