#include "MainWindow.h"
#include <QApplication>

#include "DbConnection.h"
#include "EntityFactory.h"
#include "StateManager.h"
#include "StartState.h"
#include "MainState.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	
	// Establish connection with database 
	DbConnection::DatabaseConnection(true);
	
	// Initiate entity factory
	EntityFactory* factory = EntityFactory::EntityFactoryInstance();
	factory->StartupFactory();
	
	
	StateManager* manager = StateManager::StateManagerInstance();
	manager->changeState(new MainState());

	MainWindow w;
	w.ChangeWindowState(manager->getCurrentState());

    w.show();
		
    return a.exec();
}
