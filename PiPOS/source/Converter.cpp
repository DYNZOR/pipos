#include "Converter.h"

Converter* Converter::pConverter;

Converter::Converter()
{
	sm_MapProductCategories[QString("Beers")] = Beers;
	sm_MapProductCategories[QString("Foods")] = Foods;
	sm_MapProductCategories[QString("SoftDrinks")] = SoftDrinks;
	sm_MapProductCategories[QString("HotDrinks")] = HotDrinks;
	sm_MapProductCategories[QString("Spirits")] = Spirits;
	sm_MapProductCategories[QString("Shots")] = Shots;
}

Converter::~Converter()
{
	
}

ProductCategory Converter::ConvertCategoryFromString(QString productCategoryIn)
{
	ProductCategory category; 
	
	switch (sm_MapProductCategories[productCategoryIn])
	{
	case Foods:
		category = Foods;
		break;
	case Beers:
		category = Beers;
		break;
	case SoftDrinks:
		category = SoftDrinks;
		break;
	case HotDrinks:
		category = HotDrinks;
		break;
	case Wines:
		category = Wines;
		break;
	case Spirits:
		category = Spirits;
		break;
	case Shots:
		category = Shots;
		break;
	default:
		category = Undefined;
		break;
	}
	
	return category;
}

TaxCategory Converter::ConvertTaxFromString(QString productCategoryIn)
{
	
}
