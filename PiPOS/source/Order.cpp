#include "Order.h"

Order::Order()
{
	
}

Order::Order(QString s_StatusIn)
{
	OrderItemsModel = new QSqlQueryModel();
	s_Status = s_StatusIn;
	
	// Retrieves the order from the database into dynamic memory. 
	RetrieveOrder();

}

Order::~Order()
{
	
}

void Order::RetrieveOrder()
{
	QSqlQuery query; 
	
	// If the order is new 
	if (s_Status == QString("NEW"))
	{
		// Insert a new order into the table 
		query.prepare("INSERT INTO orders (status) VALUES (:status)");
		query.bindValue(":status", s_Status);
		if (!query.exec()) qDebug() << query.lastError().text();

		// Obtain the ID of the newly inserted order
		if (!query.exec("SELECT last_insert_rowid()")) qDebug() << query.lastError().text();
		query.first();
		ui_OrderID = query.value(0).toInt();
	}
}

void Order::AddProduct(Product* newProduct)
{
	// Create an iterator to traverse the map to look for the product ID. 
	std::map<unsigned int, unsigned int>::const_iterator it = productQuantityMap.find(newProduct->GetID());
	
	// If the iterator doesn't reach the end of the map, the product already exists.
	if (it != productQuantityMap.end())
	{
		// Increment the product count. 
		productQuantityMap[newProduct->GetID()] += 1;
		
		// Update database entry of product with new quantity. 
		QSqlQuery updateProductQuery; 
		updateProductQuery.prepare("UPDATE order_items SET quantity = :qty WHERE order_id = :o_id AND product_id = :p_id" );
		updateProductQuery.bindValue(":o_id", ui_OrderID);
		updateProductQuery.bindValue(":p_id", newProduct->GetID());
		updateProductQuery.bindValue(":qty", productQuantityMap[newProduct->GetID()]);
		
		if (!updateProductQuery.exec()) qDebug() << updateProductQuery.lastError().text();
	} 
	else 
	{
		// Set the product count to 1. 
		productQuantityMap[newProduct->GetID()] = 1;
		
		// Insert the new product into the database table of order items.
		QSqlQuery insertProductQuery; 
		insertProductQuery.prepare("INSERT INTO order_items (order_id, product_id, quantity) VALUES (:o_id, :p_id, :qty)");
		insertProductQuery.bindValue(":o_id", ui_OrderID);
		insertProductQuery.bindValue(":p_id", newProduct->GetID());
		insertProductQuery.bindValue(":qty", productQuantityMap[newProduct->GetID()]);
		
		if (!insertProductQuery.exec()) qDebug() << insertProductQuery.lastError().text();

	}
	
	// Append product to order list 
	a_ProductList.append(newProduct);
	
	UpdateOrderItemsModel();
}

void Order::UpdateOrderItemsModel()
{
	QSqlQuery selectProductItemsQuery;
	selectProductItemsQuery.prepare("SELECT * FROM order_items WHERE order_id = :o_id");
	selectProductItemsQuery.bindValue(":o_id", ui_OrderID);
	
	if (!selectProductItemsQuery.exec()) qDebug() << selectProductItemsQuery.lastError().text();
	
	OrderItemsModel->setQuery(selectProductItemsQuery);
}


unsigned int Order::GetTotalOrderItems() //!< Returns the amount of items in the order.
{
	return a_ProductList.count();
}

float Order::GetTotalOrderPrice() //!< Returns the total order price.
{
	
}

QSqlQueryModel* Order::GetOrderItemsModel() //!< Returns the query items model of the order. 
{
	return OrderItemsModel;
}
