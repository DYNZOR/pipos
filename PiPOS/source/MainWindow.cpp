#include "MainWindow.h"
#include "ui/ui_MainWindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ConnectSignalsToSlots();
		
	// Updates the table view with current products model 
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}
		
	
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ChangeWindowState(State* newState)
{
	ui->MainWidget->setCurrentIndex(newState->getType());
}

void MainWindow::OpenPayment()
{
	
}

void MainWindow::OpenSettings()
{
	
}

void MainWindow::OpenTableManagement()
{
	
}

void MainWindow::OpenTabManagement()
{
	
}

void MainWindow::Exit()
{
	
}

void MainWindow::SelectDeals()
{
	QSqlQuery selectDealsQuery;
	selectDealsQuery.prepare("SELECT * FROM products WHERE type='" + QString("Deal") + "'");
	
	// Update the products table view 
	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectDealsQuery);
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());

}

void MainWindow::SelectFoods()
{
//	QSqlQuery selectDealsQuery;
//	selectDealsQuery.prepare("SELECT * FROM products WHERE type='" + QString("Deal") + "'");
//	
//	// Update the products table view 
//	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectDealsQuery);
//	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}

void MainWindow::SelectBeers()
{
	QSqlQuery selectBeersQuery;
	selectBeersQuery.prepare("SELECT * FROM products WHERE type='" + QString("Beers") + "'");
	
	// Update the products table view 
	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectBeersQuery);
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}

void MainWindow::SelectWines()
{
	QSqlQuery selectWinesQuery;
	selectWinesQuery.prepare("SELECT * FROM products WHERE type='" + QString("Wines") + "'");
	
	// Update the products table view 
	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectWinesQuery);
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}

void MainWindow::SelectSpirits()
{
	QSqlQuery selectSpiritsQuery;
	selectSpiritsQuery.prepare("SELECT * FROM products WHERE type='" + QString("Spirits") + "'");
	
	// Update the products table view 
	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectSpiritsQuery);
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}

void MainWindow::SelectShots()
{
	QSqlQuery selectShotsQuery;
	selectShotsQuery.prepare("SELECT * FROM products WHERE type='" + QString("Shots") + "'");
	
	// Update the products table view 
	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectShotsQuery);
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}

void MainWindow::SelectHotDrinks()
{
	QSqlQuery selectHotDrinksQuery;
	selectHotDrinksQuery.prepare("SELECT * FROM products WHERE type='" + QString("HotDrinks") + "'");
	
	// Update the products table view 
	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectHotDrinksQuery);
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}

void MainWindow::SelectSoftDrinks()
{
	QSqlQuery selectSoftDrinksQuery;
	selectSoftDrinksQuery.prepare("SELECT * FROM products WHERE type='" + QString("SoftDrinks") + "'");
	
	// Update the products table view 
	EntityFactory::EntityFactoryInstance()->UpdateProductsModelView(selectSoftDrinksQuery);
	ui->ProductsView->setModel(EntityFactory::EntityFactoryInstance()->GetProductsModel());
}

void MainWindow::SelectProductFromTable(const QModelIndex& index) 
{
	// Obtain ID of product from the selected row 
	QSqlRecord record = EntityFactory::EntityFactoryInstance()->GetProductsModel()->record(index.row());
	unsigned int tempProductId = record.value("id").toInt();
	
	ui->NumberOfItemsLineEdit->setText(QString::number(tempProductId)); // for testing purposes
	
	// Use ID to select correct product from the database table 
	QSqlQuery selectProductByIDQuery;
	selectProductByIDQuery.prepare("SELECT * FROM products WHERE id='" + QString::number(tempProductId) + "'");
	
	if (!selectProductByIDQuery.exec())  qDebug() << selectProductByIDQuery.lastError().text();
	
	selectProductByIDQuery.first();
	
	QString s_NewDescription = selectProductByIDQuery.value(1).toString();
	float f_Price = selectProductByIDQuery.value(2).toFloat();
	QString productCat = selectProductByIDQuery.value(3).toString();
	QString taxCat = selectProductByIDQuery.value(4).toString();
	
	// Create the new product object. 
	Product* selectedProduct = new Product(tempProductId, s_NewDescription, f_Price);
	selectedProduct->SetCategory(productCat);
	
	// Add new product to active order. 
	EntityFactory::EntityFactoryInstance()->GetActiveOrder()->AddProduct(selectedProduct);
	
	// Update the order view with items model from the order. 
	ui->OrderView->setModel(EntityFactory::EntityFactoryInstance()->GetActiveOrder()->GetOrderItemsModel());	
}

void MainWindow::ConnectSignalsToSlots()
{
	connect(ui->DealsBtn, SIGNAL(clicked()), this, SLOT(SelectDeals()));
	connect(ui->FoodsBtn, SIGNAL(clicked()), this, SLOT(SelectFoods()));
	connect(ui->SoftDrinksBtn, SIGNAL(clicked()), this, SLOT(SelectSoftDrinks()));
	connect(ui->HotDrinksBtn, SIGNAL(clicked()), this, SLOT(SelectHotDrinks()));
	connect(ui->BeersBtn, SIGNAL(clicked()), this, SLOT(SelectBeers()));
	connect(ui->WinesBtn, SIGNAL(clicked()), this, SLOT(SelectWines()));
	connect(ui->SpiritsBtn, SIGNAL(clicked()), this, SLOT(SelectSpirits()));
	connect(ui->ShotsBtn, SIGNAL(clicked()), this, SLOT(SelectShots()));	
	
	connect(ui->ProductsView, SIGNAL(clicked(QModelIndex)), this, SLOT(SelectProductFromTable(const QModelIndex&)));

}

