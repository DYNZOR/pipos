#ifndef PRODUCT_H
#define PRODUCT_H

#include <QString>
#include "Converter.h"

class Product
{
private:

	unsigned int ui_ProductId; 
	QString s_Description;
	float f_Price;
	ProductCategory productCat;
	TaxCategory taxCat;
	
protected:

public:

	Product();
	Product(unsigned int ui_ProductIdIn, QString s_DescIn, float f_PriceIn);
	~Product();

	void SetDescription(QString s_DescIn);
	void SetCategory(QString catIn);
	void SetTaxCategory(TaxCategory taxCatIn);
	void SetPrice(float f_PriceIn);
		
	unsigned int GetID();
	QString GetDescription();
	float GetPrice();
	
};

#endif 