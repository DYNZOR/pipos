#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlDriver>

class DbConnection {

public:

	static DbConnection* DatabaseConnection(bool bIsOffline) {
		if (!pDbConnection) {

			return pDbConnection = new DbConnection(bIsOffline);
		}
		else {
			return pDbConnection;
		}
	}
	
	QSqlDatabase getHandle(); //!< Obtain database handle. 

	bool isConnected(); //!< Check to to see if connection was succesfull. 

protected:

	DbConnection(bool bIsOffline);
	~DbConnection();

private: 
	static DbConnection* pDbConnection;

	QSqlDatabase db; //!< QT database handle 

	QString sHostName; //!< Mysql database hostname 
	QString sDatabaseName; //!< MySQL database name 
	QString sUserName; //!< MySQL database username login
	QString sPassword; //!< MySQL database password 
	
	QString sDbPath; //!< SQLite database path. 
	
	bool bIsOfflineDatabase; //!< True if using SQLite db, false if MySQL. 
	
	bool bConnectionEstablished; //!< Returns false if connection has failed, true if succesfull. 
	
	bool performConnection();
};

#endif
