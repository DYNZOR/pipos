#ifndef MAINSTATE_H
#define MAINSTATE_H

#include "State.h"

class MainState: public State
{
private:
	
protected:
	
public:
	
	MainState()
	{
		setType(MAINSCREEN);
	}
};

#endif 