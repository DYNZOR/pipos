#ifndef ENTITYFACTORY_H
#define ENTITYFACTORY_H

#include <vector>
#include <QtSql/QSqlQueryModel>
#include <QtSql/QSqlQuery>

#include "Product.h"
#include "Order.h"
#include "Deal.h"
#include "Receipt.h"

class EntityFactory
{

private:
	
	static EntityFactory* pEntityFactory;
	
	std::vector<Product*> a_Products;
//	std::vector<Receipt> a_Receipts;
//	std::vector<Deal> a_Deals;
//	std::vector<Report> a_Reports;
	
	// QueryModels for storing query results 
	QSqlQueryModel *productsModel; 
	
	Order* p_ActiveOrder; //!< Pointer to the current active order on display 
	
	
protected:

	EntityFactory() {

	}

public:
	
	static EntityFactory* EntityFactoryInstance() {
		if (!pEntityFactory) {
			return pEntityFactory = new EntityFactory();
		}
		else {
			return pEntityFactory;
		}

	} 
	
	// Factory initiation and shutdown routines
	void StartupFactory();
	void ShutdownFactory();
	
	// Entity creation functions
	void CreateOrder(bool bIsNew);
	void CreateProduct(const Product& newProduct);
	void CreateReceipt();	
	void CreateReport();
	
	void UpdateProductsModelView(QSqlQuery queryIn);
	
	QSqlQueryModel* GetProductsModel(); //!< Returns the products query model.
	Order* GetActiveOrder();
	
};

#endif 