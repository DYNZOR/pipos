#ifndef STARTSTATE_H
#define STARTSTATE_H

#include "State.h"

class StartState : public State
{

private:
	
protected:
	
public:
	StartState() 
	{
		setType(LOGIN);
	}
};

#endif 