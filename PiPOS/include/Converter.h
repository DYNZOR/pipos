#ifndef CONVERTER_H
#define CONVERTER_H

#include <QString>
#include <map>

enum ProductCategory
{
	Undefined,
	Deals,
	Foods,
	Beers,
	SoftDrinks,
	HotDrinks,
	Wines,
	Spirits, 
	Shots
};

enum TaxCategory
{
	TaxExempt
};

// The purpose of this class is to provide conversion utilities between enumerators and strings. 
class Converter
{
private:
	
	static Converter* pConverter; 
	std::map<QString, ProductCategory> sm_MapProductCategories; //!< Map that associates string values to product category enums.
	
protected:
	Converter();
	~Converter();

public:

	static Converter* ConverterInstance() {
		if (!pConverter) {

			return pConverter = new Converter();
		}
		else {
			return pConverter;
		}
	}

	ProductCategory ConvertCategoryFromString(QString productCategoryIn);
	TaxCategory ConvertTaxFromString(QString productCategoryIn);

};

#endif 