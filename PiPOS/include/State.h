#ifndef STATE_H
#define STATE_H

enum Type {
	LOGIN,	//!< Widget Index is equal to 0
	MAINSCREEN, //!< Widget Index is equal to 1
};

class State
{
protected:
	
	Type stateType;
	
	State()
	{
		
	}
	
public:

	void setType(Type newType)
	{
		stateType = newType;
	}
	
	Type getType()
	{
		return stateType;
	}
};

#endif 