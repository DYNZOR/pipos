#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "State.h"
#include "DbConnection.h"
#include "EntityFactory.h"
#include <QtSql/QSqlRecord>
#include <QDebug>
#include <QtSql/QSqlError>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
	
	void ChangeWindowState(State* newState); //!< Changes the ui state of window 
		
	
protected slots:
	
	////////////////////////////////////////////////////////////////
	// MainState slot functions for corresponding button triggers //
	////////////////////////////////////////////////////////////////
	void OpenPayment();
	void OpenSettings();
	void OpenTableManagement();
	void OpenTabManagement();
	void Exit();
	
	void SelectDeals();
	void SelectFoods();
	void SelectBeers();
	void SelectWines();
	void SelectSpirits();
	void SelectShots();
	void SelectHotDrinks();
	void SelectSoftDrinks();
	
	void SelectProductFromTable(const QModelIndex& index); //!< Selects a product by its ID from the table and adds it to order list 
	
private:
    Ui::MainWindow *ui;
	
	void ConnectSignalsToSlots();

};

#endif // MAINWINDOW_H
