#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include "State.h"

class StateManager
{

private:
	
	static StateManager* stateManager;
	
	State* pActiveState;

protected:

	StateManager() {
		//pActiveState = nullptr;
	}

public:
	
	static StateManager* StateManagerInstance() {
		if (!stateManager) {
			return stateManager = new StateManager();
		}
		else {
			return stateManager;

		}

	}  //!< Returns a pointer to a StateManager instance

	// Setup Contructor 
	void changeState(State* newState); 

	void showDialog(State* dialog);
	void closeDialog();
	void pushState(State* newState);
	void popState(State* newState);

	State* getCurrentState();

};

#endif 