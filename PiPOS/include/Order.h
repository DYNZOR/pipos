#ifndef ORDER_H
#define ORDER_H

#include "Product.h"
#include <QList>
#include <QString>
#include <QtSql/QSqlQueryModel>
#include <QtSql/QSqlQuery>
#include <QDebug>
#include <QtSql/QSqlError>

class Order
{
private:
	
	
	QList<Product*> a_ProductList; //!< List of products attached to this order. 
	std::map<unsigned int /* Quantity */, unsigned int /* Product ID */> productQuantityMap; //!< A map that stores the quantity of each product in the order. 
	
	unsigned int ui_OrderID; //!< Unique Order ID. 
	QString s_Status; //!< Status of order, can be new, outstanding or paid. 

	unsigned int ui_TotalOrderItems; //!< Total number of items in order. 
	float f_TotalOrderPrice; //!< Total price of the order. 
	
	void RetrieveOrder();

	QSqlQueryModel* OrderItemsModel; //!< Model for storing query results. 
	void UpdateOrderItemsModel();

protected:

public:

	Order();
	Order(QString s_StatusIn);
	~Order();
	void AddProduct(Product* newProduct); //!< Adds a product to the order. 
	
	unsigned int GetTotalOrderItems(); //!< Returns the amount of items in the order.
	float GetTotalOrderPrice(); //!< Returns the total order price. 
	QSqlQueryModel* GetOrderItemsModel(); //!< Returns the query items model of the order. 
	
};

#endif 