/********************************************************************************
** Form generated from reading UI file 'PaymentWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PAYMENTWINDOW_H
#define UI_PAYMENTWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PaymentWindow
{
public:
    QTabWidget *tabWidget;
    QWidget *cash_tab;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QLabel *total_label_3;
    QLineEdit *total_lineedit_2;
    QLabel *total_label_4;
    QLineEdit *remaining_lineedit_2;
    QWidget *tab_2;
    QLabel *total_label;
    QLineEdit *total_lineedit;
    QLabel *total_label_2;
    QLineEdit *remaining_lineedit;

    void setupUi(QWidget *PaymentWindow)
    {
        if (PaymentWindow->objectName().isEmpty())
            PaymentWindow->setObjectName(QStringLiteral("PaymentWindow"));
        PaymentWindow->resize(630, 462);
        tabWidget = new QTabWidget(PaymentWindow);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 50, 631, 411));
        QFont font;
        font.setPointSize(18);
        tabWidget->setFont(font);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setIconSize(QSize(20, 20));
        tabWidget->setElideMode(Qt::ElideNone);
        tabWidget->setUsesScrollButtons(true);
        tabWidget->setTabsClosable(false);
        tabWidget->setMovable(false);
        cash_tab = new QWidget();
        cash_tab->setObjectName(QStringLiteral("cash_tab"));
        pushButton = new QPushButton(cash_tab);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(80, 30, 111, 71));
        pushButton_2 = new QPushButton(cash_tab);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(200, 30, 111, 71));
        pushButton_3 = new QPushButton(cash_tab);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(320, 30, 111, 71));
        pushButton_4 = new QPushButton(cash_tab);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(440, 30, 111, 71));
        pushButton_5 = new QPushButton(cash_tab);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(320, 110, 111, 71));
        pushButton_6 = new QPushButton(cash_tab);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(80, 110, 111, 71));
        pushButton_7 = new QPushButton(cash_tab);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(440, 110, 111, 71));
        pushButton_8 = new QPushButton(cash_tab);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setGeometry(QRect(200, 110, 111, 71));
        pushButton_9 = new QPushButton(cash_tab);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));
        pushButton_9->setGeometry(QRect(320, 190, 111, 71));
        pushButton_10 = new QPushButton(cash_tab);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));
        pushButton_10->setGeometry(QRect(80, 190, 111, 71));
        pushButton_11 = new QPushButton(cash_tab);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));
        pushButton_11->setGeometry(QRect(440, 190, 111, 71));
        pushButton_12 = new QPushButton(cash_tab);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));
        pushButton_12->setGeometry(QRect(200, 190, 111, 71));
        total_label_3 = new QLabel(cash_tab);
        total_label_3->setObjectName(QStringLiteral("total_label_3"));
        total_label_3->setGeometry(QRect(130, 300, 55, 31));
        QFont font1;
        font1.setPointSize(12);
        total_label_3->setFont(font1);
        total_lineedit_2 = new QLineEdit(cash_tab);
        total_lineedit_2->setObjectName(QStringLiteral("total_lineedit_2"));
        total_lineedit_2->setGeometry(QRect(190, 300, 113, 31));
        total_lineedit_2->setReadOnly(true);
        total_label_4 = new QLabel(cash_tab);
        total_label_4->setObjectName(QStringLiteral("total_label_4"));
        total_label_4->setGeometry(QRect(320, 300, 101, 31));
        total_label_4->setFont(font1);
        remaining_lineedit_2 = new QLineEdit(cash_tab);
        remaining_lineedit_2->setObjectName(QStringLiteral("remaining_lineedit_2"));
        remaining_lineedit_2->setGeometry(QRect(400, 300, 113, 31));
        remaining_lineedit_2->setReadOnly(true);
        tabWidget->addTab(cash_tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        tabWidget->addTab(tab_2, QString());
        total_label = new QLabel(PaymentWindow);
        total_label->setObjectName(QStringLiteral("total_label"));
        total_label->setGeometry(QRect(120, 10, 55, 31));
        total_label->setFont(font1);
        total_lineedit = new QLineEdit(PaymentWindow);
        total_lineedit->setObjectName(QStringLiteral("total_lineedit"));
        total_lineedit->setGeometry(QRect(180, 10, 113, 31));
        total_lineedit->setReadOnly(true);
        total_label_2 = new QLabel(PaymentWindow);
        total_label_2->setObjectName(QStringLiteral("total_label_2"));
        total_label_2->setGeometry(QRect(310, 10, 101, 31));
        total_label_2->setFont(font1);
        remaining_lineedit = new QLineEdit(PaymentWindow);
        remaining_lineedit->setObjectName(QStringLiteral("remaining_lineedit"));
        remaining_lineedit->setGeometry(QRect(420, 10, 113, 31));
        remaining_lineedit->setReadOnly(true);

        retranslateUi(PaymentWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PaymentWindow);
    } // setupUi

    void retranslateUi(QWidget *PaymentWindow)
    {
        PaymentWindow->setWindowTitle(QApplication::translate("PaymentWindow", "Payment", 0));
        pushButton->setText(QApplication::translate("PaymentWindow", "#50", 0));
        pushButton_2->setText(QApplication::translate("PaymentWindow", "#20", 0));
        pushButton_3->setText(QApplication::translate("PaymentWindow", "#10", 0));
        pushButton_4->setText(QApplication::translate("PaymentWindow", "#5", 0));
        pushButton_5->setText(QApplication::translate("PaymentWindow", "50p", 0));
        pushButton_6->setText(QApplication::translate("PaymentWindow", "#2", 0));
        pushButton_7->setText(QApplication::translate("PaymentWindow", "20p", 0));
        pushButton_8->setText(QApplication::translate("PaymentWindow", "#1", 0));
        pushButton_9->setText(QApplication::translate("PaymentWindow", "2p", 0));
        pushButton_10->setText(QApplication::translate("PaymentWindow", "10p", 0));
        pushButton_11->setText(QApplication::translate("PaymentWindow", "1p", 0));
        pushButton_12->setText(QApplication::translate("PaymentWindow", "5p", 0));
        total_label_3->setText(QApplication::translate("PaymentWindow", "Given", 0));
        total_label_4->setText(QApplication::translate("PaymentWindow", "Change", 0));
        tabWidget->setTabText(tabWidget->indexOf(cash_tab), QApplication::translate("PaymentWindow", "Cash", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("PaymentWindow", "Card", 0));
        total_label->setText(QApplication::translate("PaymentWindow", "Total", 0));
        total_label_2->setText(QApplication::translate("PaymentWindow", "Remaining", 0));
    } // retranslateUi

};

namespace Ui {
    class PaymentWindow: public Ui_PaymentWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PAYMENTWINDOW_H
