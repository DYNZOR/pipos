/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *MainWidget;
    QWidget *LoginPage;
    QPushButton *pushButton;
    QWidget *MainPage;
    QPushButton *PaymentBtn;
    QPushButton *FoodsBtn;
    QPushButton *BeersBtn;
    QPushButton *SoftDrinksBtn;
    QPushButton *DealsBtn;
    QPushButton *HotDrinksBtn;
    QPushButton *ViewTabsBtn;
    QLabel *OrderListTitleLabel;
    QLabel *NumberOfItemsLabel;
    QPushButton *WinesBtn;
    QPushButton *SpiritsBtn;
    QPushButton *ShotsBtn;
    QPushButton *LogOutBtn;
    QLineEdit *NumberOfItemsLineEdit;
    QLineEdit *TotalAmountLineEdit;
    QLabel *TotalAmountLabel;
    QPushButton *ManageTablesBtn;
    QPushButton *AddToTabBtn;
    QPushButton *RefreshOrderListBtn;
    QTableView *ProductsView;
    QPushButton *SettingsBtn;
    QPushButton *InventoryBtn;
    QPushButton *SalesBtn;
    QLabel *OrderIDLabel;
    QTableView *OrderView;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1024, 768);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        MainWidget = new QStackedWidget(centralWidget);
        MainWidget->setObjectName(QStringLiteral("MainWidget"));
        MainWidget->setEnabled(true);
        MainWidget->setGeometry(QRect(-10, 0, 1024, 751));
        LoginPage = new QWidget();
        LoginPage->setObjectName(QStringLiteral("LoginPage"));
        pushButton = new QPushButton(LoginPage);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(540, 230, 75, 23));
        MainWidget->addWidget(LoginPage);
        MainPage = new QWidget();
        MainPage->setObjectName(QStringLiteral("MainPage"));
        PaymentBtn = new QPushButton(MainPage);
        PaymentBtn->setObjectName(QStringLiteral("PaymentBtn"));
        PaymentBtn->setGeometry(QRect(260, 570, 231, 80));
        FoodsBtn = new QPushButton(MainPage);
        FoodsBtn->setObjectName(QStringLiteral("FoodsBtn"));
        FoodsBtn->setGeometry(QRect(640, 10, 111, 41));
        BeersBtn = new QPushButton(MainPage);
        BeersBtn->setObjectName(QStringLiteral("BeersBtn"));
        BeersBtn->setGeometry(QRect(520, 60, 111, 41));
        SoftDrinksBtn = new QPushButton(MainPage);
        SoftDrinksBtn->setObjectName(QStringLiteral("SoftDrinksBtn"));
        SoftDrinksBtn->setGeometry(QRect(760, 10, 111, 41));
        DealsBtn = new QPushButton(MainPage);
        DealsBtn->setObjectName(QStringLiteral("DealsBtn"));
        DealsBtn->setGeometry(QRect(520, 10, 111, 41));
        HotDrinksBtn = new QPushButton(MainPage);
        HotDrinksBtn->setObjectName(QStringLiteral("HotDrinksBtn"));
        HotDrinksBtn->setGeometry(QRect(880, 10, 111, 41));
        ViewTabsBtn = new QPushButton(MainPage);
        ViewTabsBtn->setObjectName(QStringLiteral("ViewTabsBtn"));
        ViewTabsBtn->setGeometry(QRect(20, 570, 111, 81));
        OrderListTitleLabel = new QLabel(MainPage);
        OrderListTitleLabel->setObjectName(QStringLiteral("OrderListTitleLabel"));
        OrderListTitleLabel->setGeometry(QRect(140, 0, 191, 31));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        OrderListTitleLabel->setFont(font);
        OrderListTitleLabel->setAlignment(Qt::AlignCenter);
        NumberOfItemsLabel = new QLabel(MainPage);
        NumberOfItemsLabel->setObjectName(QStringLiteral("NumberOfItemsLabel"));
        NumberOfItemsLabel->setGeometry(QRect(260, 410, 111, 31));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setWeight(75);
        NumberOfItemsLabel->setFont(font1);
        NumberOfItemsLabel->setAlignment(Qt::AlignCenter);
        WinesBtn = new QPushButton(MainPage);
        WinesBtn->setObjectName(QStringLiteral("WinesBtn"));
        WinesBtn->setGeometry(QRect(640, 60, 111, 41));
        SpiritsBtn = new QPushButton(MainPage);
        SpiritsBtn->setObjectName(QStringLiteral("SpiritsBtn"));
        SpiritsBtn->setGeometry(QRect(760, 60, 111, 41));
        ShotsBtn = new QPushButton(MainPage);
        ShotsBtn->setObjectName(QStringLiteral("ShotsBtn"));
        ShotsBtn->setGeometry(QRect(880, 60, 111, 41));
        LogOutBtn = new QPushButton(MainPage);
        LogOutBtn->setObjectName(QStringLiteral("LogOutBtn"));
        LogOutBtn->setGeometry(QRect(20, 660, 111, 81));
        NumberOfItemsLineEdit = new QLineEdit(MainPage);
        NumberOfItemsLineEdit->setObjectName(QStringLiteral("NumberOfItemsLineEdit"));
        NumberOfItemsLineEdit->setGeometry(QRect(380, 410, 121, 31));
        TotalAmountLineEdit = new QLineEdit(MainPage);
        TotalAmountLineEdit->setObjectName(QStringLiteral("TotalAmountLineEdit"));
        TotalAmountLineEdit->setGeometry(QRect(380, 450, 121, 31));
        TotalAmountLabel = new QLabel(MainPage);
        TotalAmountLabel->setObjectName(QStringLiteral("TotalAmountLabel"));
        TotalAmountLabel->setGeometry(QRect(260, 450, 111, 31));
        TotalAmountLabel->setFont(font1);
        TotalAmountLabel->setAlignment(Qt::AlignCenter);
        ManageTablesBtn = new QPushButton(MainPage);
        ManageTablesBtn->setObjectName(QStringLiteral("ManageTablesBtn"));
        ManageTablesBtn->setGeometry(QRect(140, 570, 111, 81));
        AddToTabBtn = new QPushButton(MainPage);
        AddToTabBtn->setObjectName(QStringLiteral("AddToTabBtn"));
        AddToTabBtn->setGeometry(QRect(20, 410, 111, 41));
        RefreshOrderListBtn = new QPushButton(MainPage);
        RefreshOrderListBtn->setObjectName(QStringLiteral("RefreshOrderListBtn"));
        RefreshOrderListBtn->setGeometry(QRect(140, 410, 111, 41));
        ProductsView = new QTableView(MainPage);
        ProductsView->setObjectName(QStringLiteral("ProductsView"));
        ProductsView->setGeometry(QRect(520, 120, 511, 621));
        ProductsView->setSelectionBehavior(QAbstractItemView::SelectRows);
        ProductsView->horizontalHeader()->setVisible(true);
        ProductsView->horizontalHeader()->setHighlightSections(false);
        ProductsView->verticalHeader()->setVisible(false);
        SettingsBtn = new QPushButton(MainPage);
        SettingsBtn->setObjectName(QStringLiteral("SettingsBtn"));
        SettingsBtn->setGeometry(QRect(140, 660, 111, 81));
        InventoryBtn = new QPushButton(MainPage);
        InventoryBtn->setObjectName(QStringLiteral("InventoryBtn"));
        InventoryBtn->setGeometry(QRect(260, 660, 111, 81));
        SalesBtn = new QPushButton(MainPage);
        SalesBtn->setObjectName(QStringLiteral("SalesBtn"));
        SalesBtn->setGeometry(QRect(380, 660, 111, 81));
        OrderIDLabel = new QLabel(MainPage);
        OrderIDLabel->setObjectName(QStringLiteral("OrderIDLabel"));
        OrderIDLabel->setGeometry(QRect(330, 0, 61, 31));
        OrderIDLabel->setFont(font);
        OrderIDLabel->setAlignment(Qt::AlignCenter);
        OrderView = new QTableView(MainPage);
        OrderView->setObjectName(QStringLiteral("OrderView"));
        OrderView->setGeometry(QRect(20, 30, 481, 371));
        OrderView->verticalHeader()->setVisible(false);
        MainWidget->addWidget(MainPage);
        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        retranslateUi(MainWindow);
        QObject::connect(pushButton, SIGNAL(clicked()), MainWindow, SLOT(ButtonClicked()));

        MainWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PiPOS", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Press me", 0));
        PaymentBtn->setText(QApplication::translate("MainWindow", "Payment", 0));
        FoodsBtn->setText(QApplication::translate("MainWindow", "Food", 0));
        BeersBtn->setText(QApplication::translate("MainWindow", "Beers", 0));
        SoftDrinksBtn->setText(QApplication::translate("MainWindow", "Soft Drinks", 0));
        DealsBtn->setText(QApplication::translate("MainWindow", "Deals", 0));
        HotDrinksBtn->setText(QApplication::translate("MainWindow", "Hot Drinks", 0));
        ViewTabsBtn->setText(QApplication::translate("MainWindow", "Ongoing Tabs", 0));
        OrderListTitleLabel->setText(QApplication::translate("MainWindow", "Current Order:", 0));
        NumberOfItemsLabel->setText(QApplication::translate("MainWindow", "Total # Items", 0));
        WinesBtn->setText(QApplication::translate("MainWindow", "Wines", 0));
        SpiritsBtn->setText(QApplication::translate("MainWindow", "Spirits", 0));
        ShotsBtn->setText(QApplication::translate("MainWindow", "Shots", 0));
        LogOutBtn->setText(QApplication::translate("MainWindow", "Log Out", 0));
        TotalAmountLabel->setText(QApplication::translate("MainWindow", "Total Amount", 0));
        ManageTablesBtn->setText(QApplication::translate("MainWindow", "Manage Tables", 0));
        AddToTabBtn->setText(QApplication::translate("MainWindow", "Add to Tab", 0));
        RefreshOrderListBtn->setText(QApplication::translate("MainWindow", "New", 0));
        SettingsBtn->setText(QApplication::translate("MainWindow", "Settings", 0));
        InventoryBtn->setText(QApplication::translate("MainWindow", "Inventory", 0));
        SalesBtn->setText(QApplication::translate("MainWindow", "Sales", 0));
        OrderIDLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
